import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

	public static void main(String[] args) {
		/*
		 * parametri 
		 * - tipo di archiviazione: zip,.... 
		 * - cartella da archiviare 
		 * - fileName prefix
		 * - giorni  per l'archiviazione 
		 * - giorni per la cancellazione
		 *  */

		try {
			String archiveType = args[0];
			String folder = args[1];
			String fileName = args[2];
			Logger logger = Logger.getLogger("ArchiveFilesLog");
			try {
				configureLog(logger, fileName);
				logger.log(Level.INFO, new Date() + " configurated log file");
			} catch (SecurityException | IOException e1) {
				System.out.print("Error initialized log" + e1.getMessage());
			}
			logger.log(Level.INFO, new Date() + " file name parameter: " + fileName);
			logger.log(Level.INFO, new Date() + " folder parameter:  " + folder);
			Integer archiveDays = 0;
			if (args.length > 3) {
				archiveDays = Integer.parseInt(args[3]);
				logger.log(Level.INFO, new Date() + " retention file days parameter: " + archiveDays);
				Integer deleteDays = 0;
				if (args.length > 4) {
					deleteDays = Integer.parseInt(args[4]);
					logger.log(Level.INFO, new Date() + " deleted days parameter: " + deleteDays);
				}
				ArchiveFiles files = new ArchiveFiles(folder, archiveDays, fileName, deleteDays, logger);
				files.archive(files, archiveType);
				logger.log(Level.INFO, new Date() + " task archiving files finished ");
				if (deleteDays > 0) {
					files.deleteOldFiles();
					logger.log(Level.INFO, new Date() +" task deleting files finished ");
				}

			} else {
				logger.log(Level.SEVERE, new Date() + " need argument for retention file");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.print("need arguments");
			System.exit(0);
		}

	}

	private static String initializePath() {
		Path path = Paths.get(new File("./").getAbsolutePath());
		String directory = path.getParent().toString() + "\\log\\";
		File directoryPath = new File (directory);
		if (!directoryPath.exists()) {
			directoryPath.mkdir();
		}
		return directory;
	}
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static void configureLog(Logger logger, String fileName) throws SecurityException, IOException {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		 
		FileHandler fh = new FileHandler(initializePath() + fileName + sdf.format(timestamp)+".log");
		logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);

		// the following statement is used to log any messages
		logger.info(new Date() + " Log initialized");

	}

}
