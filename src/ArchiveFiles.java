import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ArchiveFiles {
	public static long H24 = 86400;
	private static SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private String folder;
	private Integer archiveDays;
	private String fileName;
	private Integer deleteDays;
	private Logger logger;

	public ArchiveFiles(String folder, Integer days, String fileName, Integer deleteDays2, Logger logger) {
		this.folder = folder;
		this.archiveDays = days;
		this.fileName = fileName;
		this.logger = logger;
	}

	public ArchiveFiles(String folder, Integer archiveDays, String fileName, Integer deleteDays) {
		this.folder = folder;
		this.archiveDays = archiveDays;
		this.fileName = fileName;
		this.deleteDays = deleteDays;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public Integer getArchiveDays() {
		return archiveDays;
	}

	public void setArchiveDays(Integer archiveDays) {
		this.archiveDays = archiveDays;
	}

	public Integer getDeleteDays() {
		return deleteDays;
	}

	public void setDeleteDays(Integer deleteDays) {
		this.deleteDays = deleteDays;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	void archive(ArchiveFiles file, String archiveType) {
		switch (archiveType) {

		case "zip":
			try {
				List<FileData> files = searchFiles(file);
				if (files.size() != 0) {
					createZipFiles(files);
					logger.info(new Date() + " zipped " + files.size() + " files");
				} else {
					logger.info(new Date() + " no files to zip ");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, new Date() + " ERROR:  " + e.getMessage());
			} catch (Exception e1) {
				logger.log(Level.SEVERE, new Date() + " ERROR:  " + e1.getMessage());
			}

		default:
			break;
		}
	}

	public void deleteOldFiles() {
		String path = getFolder() + "\\archived\\";
		File directory = new File(path);
		if (directory.exists()) {
			File[] logFiles = directory.listFiles();
			LocalDateTime oldDate = LocalDateTime.now().minusDays(getDeleteDays());
			Date out = Date.from(oldDate.atZone(ZoneId.systemDefault()).toInstant());
			String formatOldDate = sdf1.format(out);
			if (logFiles != null) {
				logger.info(new Date() + " init deleting files ");
				// delete oldest files after theres more than 500 log files
				for (File f : logFiles) {
					Date lastModf = new Date(f.lastModified());
					String formatCurrentDate = sdf1.format(lastModf);
					if (Integer.parseInt(formatCurrentDate) < Integer.parseInt(formatOldDate)) {
						f.delete();
					}
				}
				logger.info(" deledet  " + logFiles.length + " files");
			} else {
				logger.info(" no files to delete ");
			}
		}
	}

	private List<FileData> searchFiles(ArchiveFiles file) throws IOException {
		File directory = new File(file.getFolder());
		File[] logFiles = directory.listFiles();
		List<FileData> fileArchiving = new ArrayList<FileData>();
		LocalDateTime oldDate = LocalDateTime.now().minusDays(getArchiveDays());
		Date out = Date.from(oldDate.atZone(ZoneId.systemDefault()).toInstant());
		String formatOldDate = sdf1.format(out);

		if (logFiles != null) {
			for (File f : logFiles) {
				Date lastModf = new Date(f.lastModified());
				String formatCurrentDate = sdf1.format(lastModf);
				if (Integer.parseInt(formatCurrentDate) < Integer.parseInt(formatOldDate)) {
					fileArchiving.add(new FileData(f.getAbsolutePath(), formatCurrentDate));
				}
			}
		}
		return fileArchiving;
	}

	private void createZipFiles(List<FileData> files) throws IOException {
		String path = getFolder() + "\\archived\\";
		File directory = new File(path);
		if (!directory.exists()) {
			directory.mkdir();
		}

		// controllo se ci sono date diverse nella mappa e faccio un file per ogni data
		Map<String, List<FileData>> mapFiles = organizerZip(files);
		for (String data : mapFiles.keySet()) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			FileOutputStream fos = new FileOutputStream(
					path + getFileName() + "_archived_" + data + "_" + sdf.format(timestamp) + ".zip");
			ZipOutputStream zipOut = new ZipOutputStream(fos);

			for (FileData srcFile : mapFiles.get(data)) {
				File fileToZip = new File(srcFile.getName());
				FileInputStream fis = new FileInputStream(fileToZip);
				ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
				zipOut.putNextEntry(zipEntry);
				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
				fis.close();
				fileToZip.delete();
			}
			zipOut.close();
			fos.close();
		}

		logger.info(new Date() + " created " + mapFiles.size() + " archive zip");
	}

	//creo una mappa data--> array di file raggruppando tutti i file da archiviare per data di creazione
	private Map<String, List<FileData>> organizerZip(List<FileData> files) {
		if (files != null && files.size() != 0) {
			Map<String, List<FileData>> groupByDataMap = files.stream()
					.collect(Collectors.groupingBy(FileData::getData));
			return groupByDataMap;
		}
		return null;
	}

}
