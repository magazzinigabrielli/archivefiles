
public class FileData {

	private String name;
	private String data;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public FileData(String name, String data) {
		super();
		this.name = name;
		this.data = data;
	}

	
}
